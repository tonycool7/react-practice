import React from 'react';
import Layout from "./components/layouts/Nav";
import Dashboard from "./components/dashboard/index";

const App = () => (
    <div>
      <Layout/>
      <Dashboard/>
    </div>
  );

export default App;
