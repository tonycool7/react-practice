export default [
    {
        id: 1,
        name: "Apple computer",
        price: 15000,
        imgUrl : require("../assets/images/mac.jpg"),
        description: "Very durable and beautiful"
    },
    {
        id: 2,
        name: "Razar blade computer",
        price: 20000,
        imgUrl : require("../assets/images/razar.jpeg"),
        description: "Very durable and beautiful, and has higd performance"
    },
    {
        id: 3,
        name: "Microsoft computer",
        price: 45000,
        imgUrl : require("../assets/images/microsoft.jpg"),
        description: "very slim and durable"
    },
    {
        id: 4,
        name: "Asus computer",
        price: 35000,
        imgUrl : require("../assets/images/asus.jpg"),
        description: "white super fast computer"
    }
]
