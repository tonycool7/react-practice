export default [
    {
        id: 1,
        imgUrl : require("../assets/images/slide1.jpg"),
        name : "Disease registry",
        phone : "0809739489",
        email : "disreg@reg.org"
    },
    {
        id: 2,
        imgUrl : require("../assets/images/slide2.jpg"),
        name : "Kaiglostore",
        phone : "0903039292",
        email : "contact@kaiglo.com"
    },
    {
        id: 3,
        imgUrl : require("../assets/images/stats1.jpg"),
        name : "Reaps",
        phone : "0930394839",
        email : "contact@reaps.org"
    }
];
