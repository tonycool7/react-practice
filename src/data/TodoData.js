export default [
    {
        id : 1,
        action : "Clean house",
        done: false
    },
    {
        id: 2,
        action: "Repair generator",
        done: true
    },
    {
        id : 3,
        action: "Wash the rug",
        done : false
    },
    {
        id: 4,
        action: "Barb hair",
        done : true
    },
    {
        id: 5,
        action : "Prepare for examination",
        done: false
    }
]
