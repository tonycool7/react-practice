import React, {Component} from "react";
import ProjectCard from "./ProjectCard";
import ProductCard from "./ProductCard";
import ProjectData from "../../data/projects"
import ProductData from "../../data/products"
import TodoData from "../../data/TodoData"
import TodoInput from "./TodoList"
import Increment from "./Increment"

export default class Dashboard extends Component{
    constructor(){
        super();
        this.state = {
            username : "Anthony Cool",
            projects : ProjectData,
            products : ProductData,
            todos : TodoData,
            exams : [],
            count : 0,
            isLoading : true
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
    }

    async componentDidMount(){
        try {
            const response = await fetch("http://localhost:3001/api/v1/exams");
            const data = await response.json();
            this.setState({isLoading : false, exams : data});
        }catch (e) {
            console.log(e);
        }
    }

    handleCheckbox(id){
        this.setState((prevState) => {
            return {
                todos : prevState.todos.map((todo) => {
                    if(todo.id === id){
                        return {
                            ...todo,
                            done : !todo.done
                        };
                    }
                    return todo;
                })
            }
        });
    }

    handleClick(){
        this.setState(prevState => {
            return {
                count : prevState.count + 1
            }
        });
        alert(this.state.count);
    }

    render(){
        const ProjectComponent = this.state.projects.map(
            (project) => <ProjectCard key={project.id} name={project.name}
                                      imgUrl={project.imgUrl} email={project.email}
                                      phone={project.phone} />
        );

        const ProductComponent = this.state.products.map(
            (product) => <ProductCard key={product.id} name={product.name}
                                      imgUrl={product.imgUrl} description={product.description}
                                      price={product.price} />
        );

        const ExamsComponent = this.state.exams.map(
            (exam) => (
                <div className="row" key={exam._id}>
                    <div className="col-sm-8">
                        <ul className="list-group">
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                {exam.name}
                                <span className="badge badge-primary badge-pill">{exam.pass_mark}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            )
        );

        const TodoComponent = this.state.todos.map((task) =>
            (<TodoInput key={task.id} handleChange={this.handleCheckbox} item={task}/>)
        );

        return !this.state.isLoading ? (
            <div className="container">
                <br/>
                <h1>Welcome {`${this.state.username}`}</h1>
                <p>Good morning {`Mr. ${this.state.username}`}, how may we be of help to you..?</p>
                <div className="row">
                    {ProjectComponent}
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <h4>All products</h4>
                    </div>
                    {ProductComponent}
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <h5>ToDo List</h5>
                        <form className="form">
                            {TodoComponent}
                        </form>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-sm-12">
                        <button className="btn btn-block btn-success" onClick={this.handleClick}>click</button>
                    </div>
                </div>
                <Increment/>
                <div className="col-sm-12">
                    <h1>List of all exams</h1>
                </div>
                {ExamsComponent}
                <br/>
                <br/>
            </div>
        ) : (<img className="loader" src={require('../../assets/images/loader.gif')} />);

    }

};
