import React, {Component} from "react"

export default class ProductCard extends Component{
    render(){
        return(
            <div className="product-card col-sm-3">
                <img src={this.props.imgUrl}/>
                <h4>{this.props.name}</h4>
                <h4>{this.props.price.toLocaleString("en-Us", {style : "currency", currency: "USD"})}</h4>
                <p>{this.props.description}</p>
            </div>
        )
    }
};
