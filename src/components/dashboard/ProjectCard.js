import React, {Component} from "react";

export default class Card extends Component{
    render(){
        return(
            <div className="project-card col-sm-4">
                <img src={this.props.imgUrl} />
                <h4>{this.props.name}</h4>
                <p>Phone : {this.props.phone}</p>
                <p>Email : {this.props.email}</p>
            </div>
        )
    }
}
