import React, {Component} from "react"
import randomColor from "randomcolor"

export default class Increment extends Component{
    constructor(){
        super();
        this.state = {
            count : 0,
            countColor : randomColor(),
            firstname : "",
            lastname : "",
            description : "",
            sexually_active: false,
            gender : false
        };
        this.incrementCount = this.incrementCount.bind(this);
        this.decrementCount = this.decrementCount.bind(this);
        this.inputChange = this.inputChange.bind(this);
    }

    incrementCount(){
        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            }
        })
    }

    decrementCount(){
        this.setState((prevState) => {
            return {
                count: prevState.count - 1
            }
        })
    }

    inputChange(event){
        const {name, value} = event.target;
        console.log(value)
        this.setState(
            {
                [name] : value
            }
        );
    }

    componentDidUpdate(prevProps, prevState, snapshot){
        console.log("Component updated");
    }

    shouldComponentUpdate(nextProps, nextState, nextContext){
        return true;
    }

    componentDidMount(){
        console.log("Component mounted");
    }

    render(){
        return(
            <div className="row">
                <div className="col-sm-12 text-center">
                    <h1 style={{color : this.state.countColor}}>{this.state.count}</h1>
                    <button onClick={this.incrementCount} className="btn btn-success mr-3">Increase</button>
                    <button onClick={this.decrementCount} className="btn btn-danger mr-3">Decrease</button>
                </div>
                <div className="col-sm-12">
                    <h2>Data binding on input fields</h2>
                    <h4>Firstname : {this.state.firstname}</h4>
                    <h4>Lastname : {this.state.lastname}</h4>
                    <h4>Description : {this.state.description}</h4>
                    <h4>Gender : {this.state.gender}</h4>
                    <form>
                        <div className="form-group">
                            <label className="form-label">Firstname</label>
                            <input className="form-control" type="text" inputMode="text" name="firstname" onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label className="form-label">Lastname</label>
                            <input className="form-control" type="text" inputMode="text" name="lastname" onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label className="form-label">Description</label>
                            <textarea className="form-control" inputMode="text" name="description" onChange={this.inputChange} />
                        </div>
                        <div className="form-group">
                            <label className="form-label">
                                <input className="form-check-inline custom-checkbox" type="checkbox" name="sexually_active" value={this.sexually_active} onChange={this.inputChange} />
                                Sexually active?
                            </label>
                        </div>
                        <div className="form-group">
                            <label className="form-label">
                                Gender?
                            </label>
                            <label>
                                Male
                                <input className="form-check-inline custom-radio" type="radio" value="male" name="gender" onChange={this.inputChange} />
                            </label>
                            <label className="form-label">
                                Female
                                <input className="form-check-inline custom-radio" type="radio" value="female" name="gender" onChange={this.inputChange} />
                            </label>
                        </div>
                    </form>
                </div>

            </div>
        );
    }
}
