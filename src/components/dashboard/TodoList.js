import React, {Component} from "react"

export default class TodoList extends Component{
    constructor(){
        super();
    }

    // //will be deprecated in next release
    // componentWillMount(){
    //     console.log("Component will mount");
    // }

    // //will be deprecated in next release
    // componentWillUpdate(){
    //     console.log("Component will update");
    // }

    componentDidUpdate(){
        console.log("Component did update");
    }

    componentDidMount() {
        console.log("component mounted");
        // GET the data I need to correctly display
    }

    // //will be deprecated in next release
    // componentWillReceiveProps(nextProps, nextContext) {
    //     console.log("Component received props");
    //     if (nextProps.whatever !== this.props.whatever) {
    //         // do something important here
    //     }
    // }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log("Component was updated");
        return true;
        // return true if want it to update
        // return false if not
    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }


    render(){
        console.log("Rendering component");
        return (
            <div className="form-check">
                <input type="checkbox" onChange={() => this.props.handleChange(this.props.item.id)}
                       className="form-check-input"
                       checked={this.props.item.done}/>
                <label className={`form-check-label ${this.props.item.done && "task-done"}`}>
                    {this.props.item.action}
                </label>
            </div>
        )
    }
}
